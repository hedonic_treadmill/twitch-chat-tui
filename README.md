<!--
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Twitch chat TUI client

A terminal user interface for reading [Twitch](https://twitch.tv/) chats written in Rust

`Insert image here`

# Navigation
(Esq) to quit \
(h) (or (Ctrl + h) in input mode) to toggle this popup \
arrow keys or (wasd) to navigate \
(e) to join a channel \
(Backspace) or (Delete) to part from a channel \
(Tab) to show the settings menu

# TODO
(Probably won't be done without demand)
1. Persistent settings
2. Write tests
    - testing with `tempfile`
    - write more benchmarks
    - static size assertions
3. Write own connection to server
4. General refactor

If you see any questionable design decisions, assume they have been done for the sake of macro- and microperformance

# Licences
This project is licensed under AGPL-3.0-or-later\
See [LICENCE.txt](https://codeberg.org/hedonic_treadmill/twitch-chat-tui/src/branch/main/LICENCE.txt) file for details

This project is using the following third-party crates
- [`chrono`](https://crates.io/crates/chrono) - licensed under `MIT OR Apache-2.0`
- [`color-eyre`](https://crates.io/crates/color-eyre) - licensed under `MIT OR Apache-2.0`
- [`crossterm`](https://crates.io/crates/crossterm) - licensed under `MIT`
- [`futures`](https://crates.io/crates/futures) - licensed under `MIT OR Apache-2.0`
- [`indexmap`](https://crates.io/crates/indexmap) - licensed under `Apache-2.0 OR MIT`
- [`lazy-regex`](https://crates.io/crates/lazy-regex) - licensed under `MIT`
- [`mimalloc`](https://crates.io/crates/mimalloc) - licensed under `MIT`
- [`ratatui`](https://crates.io/crates/ratatui) - licensed under `MIT`
- [`tokio`](https://crates.io/crates/tokio) - licensed under `MIT`
- [`tokio-util`](https://crates.io/crates/tokio-util) - licensed under `MIT`
- [`twitch-irc`](https://crates.io/crates/twitch-irc) - licensed under `MIT`
- [`criterion`](https://crates.io/crates/criterion) - licensed under `Apache-2.0 OR MIT`

Copies of the licences for each crate are provided 
in the according folders in the [LICENCES](https://codeberg.org/hedonic_treadmill/twitch-chat-tui/src/branch/main/LICENCES/) directory
