# Copyright 2024 hedonic_treadmill
#
# SPDX-License-Identifier: AGPL-3.0-or-later


import subprocess
from pathlib import Path
import os
import platform
import shutil


PROJECT_NAME = r"twitch-chat-tui"
RELEASE_DIR = r"target/release/"
TEMP_DIR = r".bundle/"
OUTPUT_DIR = TEMP_DIR + PROJECT_NAME


class Files:
    __slots__ = (
        "_look_for_binary",
        "binary_name",
        "current_os",
    )

    def __init__(self, current_os: str):
        global PROJECT_NAME
        if current_os == "Windows":
            self.binary_name = PROJECT_NAME + ".exe"
            self._look_for_binary = self.binary_name
        elif current_os == "Linux":
            self.binary_name = PROJECT_NAME + ".bin"
            self._look_for_binary = PROJECT_NAME
        else:
            print(f"Unsupported OS: {current_os}")
            self.binary_name = None
            self._look_for_binary = None
        self.current_os = current_os

    def _find_binary(self) -> Path | None:
        global RELEASE_DIR
        found = Path(RELEASE_DIR + self._look_for_binary)
        if not found.exists():
            print("Built binary file not found")
            return None
        return found

    def _copy_files(self, output_dir: Path, binary: Path):
        licenses = Path("LICENCES")
        readme = Path("README.md")
        bundle_licences = output_dir / "LICENCES"
        bundle_readme = output_dir / "README.md"
        shutil.copytree(licenses, bundle_licences)
        bundle_readme.touch()
        bundle_readme.write_text(readme.read_text())
        bundle_binary = output_dir / self.binary_name
        bundle_binary.touch()
        bundle_binary.write_bytes(binary.read_bytes())

    def bundle(self) -> bool:
        """Returns True if successful"""
        binary = self._find_binary()
        if binary is None:
            return False
        output_dir = Path(OUTPUT_DIR)
        self._copy_files(output_dir, binary)
        return True

    def archive(self):
        bundle_dir = Path(OUTPUT_DIR).resolve()
        if not bundle_dir.exists():
            print("Temp directory not found")
            return

        form = "zip" if self.current_os == "Windows" else "gztar"
        shutil.make_archive(
            PROJECT_NAME, format=form, root_dir=TEMP_DIR, base_dir=PROJECT_NAME
        )

    def __enter__(self):
        return self

    def __exit__(self, _exc_type, _exc_val, _exc_tb):
        shutil.rmtree(Path(TEMP_DIR))


def sanity_check() -> bool:
    """Returns whether it's sane to run the script"""
    current_dir = Path(".").parent
    src_dir = current_dir / "src"
    with os.scandir(src_dir) as it:
        src_not_empty = any(it)
    cargo = current_dir / "Cargo.toml"
    licenses = current_dir / "LICENCES"
    readme = current_dir / "README.md"
    return (
        all(file.exists() for file in (src_dir, cargo, licenses, readme))
        and src_not_empty
    )


def main():
    if not sanity_check():
        print("Project files not found")
        return
    subprocess.run(["cargo", "build", "--release"])

    files = Files(platform.system())
    if files.binary_name is None:
        return

    with files as files:
        if not files.bundle():
            return
        files.archive()
    print("Successfully bundled the application")


if __name__ == "__main__":
    main()
