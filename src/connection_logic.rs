/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

mod connection_settings;
mod extra_handling;
mod format_extra;
mod format_messages;
mod handle_message;
mod log_message;

use color_eyre::eyre;
use std::collections::HashSet;
use tokio::{
    sync::mpsc::{UnboundedReceiver, UnboundedSender},
    task::JoinHandle,
};
use tokio_util::sync::CancellationToken;
use twitch_irc::{
    login::StaticLoginCredentials,
    message::ServerMessage,
    transport::websocket::{WSTransport, TLS},
    ClientConfig, SecureWSTransport, TwitchIRCClient,
};

// Some are for benchmarking, some are for other threads
// This really needs to be refactored
pub use crate::connection_logic::{
    connection_settings::ConnectionSettings,
    extra_handling::{
        client::SendIrcMessage, connection_action::ConnectionAction, handle_badges::UserBadges,
        handle_tags::MessageTags,
    },
    handle_message::handle_message,
    log_message::clean_old_logs,
};
use crate::thread_logic::app_settings::SharedSetting;

use self::extra_handling::client::ClientWrapper;

/// # Errors
///
/// Returns `Err` when unable to send a message to another thread
fn receive_message(
    maybe_message: Option<ServerMessage>,
    action_send: &UnboundedSender<ConnectionAction>,
    settings: &ConnectionSettings,
) -> eyre::Result<bool> {
    match maybe_message {
        None => action_send.send(ConnectionAction::Error(eyre::eyre!(
            "Server closed the connection unexpectedly"
        )))?, // wait for the user to send cancellation signal
        Some(server_message) => {
            if let Some(connection_action) = handle_message(server_message, settings) {
                // Woo, side effects
                action_send.send(connection_action)?;
            } // else do nothing
        }
    }
    Ok(false)
}

async fn main_loop(
    mut incoming_messages: UnboundedReceiver<ServerMessage>,
    action_send: &UnboundedSender<ConnectionAction>,
    settings_recv: &mut UnboundedReceiver<SharedSetting>,
    token: &CancellationToken,
    client: TwitchIRCClient<WSTransport<TLS>, StaticLoginCredentials>,
) -> eyre::Result<()> {
    let mut channels: HashSet<String> = HashSet::new();
    // When changing this channel list, sync changes with src/tui_logic/app.rs
    let _: bool = channels.insert("jerma985".to_owned());
    client.set_wanted_channels(channels)?;
    // TODO: Persistent settings

    let mut settings: ConnectionSettings = ConnectionSettings::default();
    let client_wrapper: ClientWrapper = ClientWrapper::new(client);

    loop {
        tokio::select! {
            biased;
            () = token.cancelled() => return Ok(()),
            Some(shared_settings) = settings_recv.recv() => {
                settings = settings.reinitialize(&client_wrapper.inner, shared_settings, action_send)?;
            },
            maybe_message = incoming_messages.recv() => {
                if receive_message(maybe_message, action_send, &settings)? {
                    return Ok(());
                }  // else do nothing and wait for the next message
            },
        }
    }
}

async fn connection_wrapper(
    action_send: UnboundedSender<ConnectionAction>,
    mut settings_recv: UnboundedReceiver<SharedSetting>,
    cancellation_token: CancellationToken,
) -> eyre::Result<()> {
    let login_credentials = StaticLoginCredentials::anonymous();
    let config = ClientConfig::new_simple(login_credentials);
    let (incoming_messages, client) =
        TwitchIRCClient::<SecureWSTransport, StaticLoginCredentials>::new(config);

    if let Err(error) = main_loop(
        incoming_messages,
        &action_send,
        &mut settings_recv,
        &cancellation_token,
        client,
    )
    .await
    {
        cancellation_token.cancel();
        return Err(error);
    }
    Ok(())
}

#[must_use]
#[inline]
pub fn spawn_connection(
    action_send: UnboundedSender<ConnectionAction>,
    settings_recv: UnboundedReceiver<SharedSetting>,
    cancellation_token: CancellationToken,
) -> JoinHandle<eyre::Result<()>> {
    tokio::spawn(connection_wrapper(
        action_send,
        settings_recv,
        cancellation_token,
    ))
}
