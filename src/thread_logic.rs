/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

pub mod app_settings;
pub mod sent_messages;

use color_eyre::eyre;
use tokio::task::JoinHandle;

type Handle = JoinHandle<eyre::Result<()>>;

/// The order of the join handles should matter
/// Priority
/// 1. error in connection thread
/// 2. error in tui thread
/// 3. error in input thread
///
/// # Errors
///
/// Returns `Err` when either thread failed to join or something went wrong inside
pub async fn gather_threads(
    connection_join_handle: Handle,
    tui_join_handle: Handle,
    input_join_handle: Handle,
) -> eyre::Result<()> {
    match tokio::join!(connection_join_handle, tui_join_handle, input_join_handle) {
        (Ok(Ok(())), Ok(Ok(())), Ok(Ok(()))) => Ok(()),
        (Ok(Err(connection_error)), _, _) => Err(connection_error),
        (_, Ok(Err(tui_error)), _) => Err(tui_error),
        (_, _, Ok(Err(input_error))) => Err(input_error),
        (Err(join_error), _, _) | (_, Err(join_error), _) | (_, _, Err(join_error)) => {
            Err(eyre::Report::new(join_error))
        }
    }
}
