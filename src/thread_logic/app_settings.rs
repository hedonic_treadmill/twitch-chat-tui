/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

pub enum SharedSetting {
    Log,
    ChannelJoined(String),
    ChannelParted(String),
}
