/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use chrono::prelude::{DateTime, Utc};

use crate::connection_logic::{MessageTags, UserBadges};

pub enum SentFollowersOnlyMode {
    NoChange,
    TurnedOff,
    DelayMins(u64),
}

impl SentFollowersOnlyMode {
    pub const fn unwrap_or(self, default: Option<u64>) -> Option<u64> {
        match self {
            Self::NoChange => default,
            Self::TurnedOff => None,
            Self::DelayMins(mins) => Some(mins),
        }
    }
}

/// `Option` to indicate whether the state has changed
/// - `Option<bool>` is self-explanatory
/// - `Option<u64>`
///     - `None` -> No change
///     - `Some(0)` -> Turned off
///     - `Some(secs)` -> `secs` second slow model
pub struct SentRoomstate {
    pub emote_only: Option<bool>,
    pub followers_only_mins: SentFollowersOnlyMode,
    pub r9k: Option<bool>,
    pub slow_mode_secs: Option<u64>,
    pub sub_only: Option<bool>,
}

impl SentRoomstate {
    pub const fn new(
        emote_only: Option<bool>,
        followers_only_mins: SentFollowersOnlyMode,
        r9k: Option<bool>,
        slow_mode_secs: Option<u64>,
        sub_only: Option<bool>,
    ) -> Self {
        Self {
            emote_only,
            followers_only_mins,
            r9k,
            slow_mode_secs,
            sub_only,
        }
    }
}

pub enum MiscMessage {
    Roomstate(SentRoomstate),
}

pub struct SystemMessage {
    pub server_timestamp: DateTime<Utc>,
    pub channel_login: String,
    pub message_text: String,
    pub misc: Option<MiscMessage>,
}

pub struct ChatMessage {
    pub server_timestamp: DateTime<Utc>,
    pub channel_login: String,
    pub sender_login: String,
    pub message_text: String,
    // pub is_action: bool,
    pub badges: UserBadges,
    pub tags: MessageTags,
}

/* struct CsvRecord {
    // bool fields can generally only be Some(true) or None
    // Since serde serializes Some(()) as empty like None and
    // to not have lots of false values, this is the crutch
    // pub filename: String,
    pub message_command: &'static str,
    pub message_type: Option<String>,
    pub server_timestamp: String,
    pub formatted_timestamp: String,
    pub channel_login: Option<String>,
    pub channel_id: Option<String>,
    pub message_text: Option<String>,
    pub is_action: Option<bool>,
    pub sender_login: Option<String>,
    pub sender_id: Option<String>,
    pub sender_name: Option<String>, // Sender name is different from login and may not be utf-8
    pub name_color: Option<String>,
    // pub visible_badges:
    // pub emotes: String,
    pub message_id: Option<String>,
    pub is_highlighted: Option<bool>,
    pub is_new: Option<bool>,
    pub is_returning: Option<bool>,
    pub is_admin: Option<bool>,
    pub is_owner: Option<bool>,
    pub is_mod: Option<bool>,
    pub is_staff: Option<bool>,
    pub is_turbo: Option<bool>,
    pub sub_months: Option<String>,
    pub bits: Option<u64>,
    pub chat_cleared: Option<bool>,
    pub is_ban: Option<bool>,
    pub timeout_seconds: Option<u64>,
    pub recipient_id: Option<String>,
    pub recipient_login: Option<String>,
    pub recipient_name: Option<String>,
    pub sub_streak: Option<u64>,
    pub mass_gift_count: Option<u64>,
    pub sub_tier: Option<String>,
    pub raid_viewer_count: Option<u64>,
    pub sender_total_gifts: Option<u64>,
    pub system_message: Option<String>,
    // These Option<bool> fields can actually have Some(false)
    // as false indicates the mode was disabled and None means no change
    pub emote_only: Option<bool>,
    pub followers_only_min: Option<isize>,
    pub r9k: Option<bool>,
    pub slow_mode_sec: Option<u64>,
    pub subscribers_only: Option<bool>,
} */
