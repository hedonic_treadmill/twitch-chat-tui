/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use color_eyre::eyre;
use crossterm::{
    cursor,
    event::{DisableBracketedPaste, EnableBracketedPaste},
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::{
    backend::CrosstermBackend,
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    text::{Line, Span, Text},
    widgets::{Block, Borders, Clear, List, ListItem, Paragraph, Wrap},
    Frame, Terminal,
};
use std::{
    io::{self, Stderr},
    panic,
};

use crate::tui_logic::{
    app::{ChannelState, Chats, CurrentScreen, DisplayMessage, Model},
    tui_settings::{MockConnectionSettings, SelectedSetting, TimestampFormat, TuiSettings},
};

pub type CTTerminal = Terminal<CrosstermBackend<Stderr>>;

const SELECTED_BORDER: Style = Style::new().fg(Color::LightYellow);
const HELP_CONTENT: &str = concat!(
    "(Esc) to quit | ",
    "(h) to show all keys | ",
    "(Tab) to show settings",
);
const HELP_CONTENT_STAGING: &str =
    "(Esc) to quit | (Ctrl + h) to show all keys | (Ctrl + z) to cancel";
const IMPORTANT: Style = Style::new().fg(Color::Red);
const SELECTED_SETTING: Style = Style::new().bg(Color::LightGreen).fg(Color::Black);
const FOCUSED_SETTING: Style = Style::new().bg(Color::Yellow);
const LOG_CONTENT: &str = "Log messages";
const BADGE_CONTENT: &str = "Show badges";
const SETTINGS_BLOCK: Block = Block::new().borders(Borders::BOTTOM);
const POPUP_CONTENT: &str = concat!(
    "(Esq) to quit \n",
    "(h) (or (Ctrl + h) in input mode) to toggle this popup \n",
    "arrow keys or (wasd) to navigate \n",
    "(e) to join a channel \n",
    "(Backspace) or (Delete) to part from a channel \n",
    "(Ctrl + z) to cancel input mode \n",
    "(Tab) to show the settings menu ",
);

fn initialize_panic_handler() {
    let original_hook = panic::take_hook();
    panic::set_hook(Box::new(move |panic_info| {
        crossterm::execute!(io::stderr(), LeaveAlternateScreen).unwrap_or(());
        // crossterm::execute!(std::io::stderr(), DisableMouseCapture).unwrap();
        crossterm::execute!(io::stderr(), DisableBracketedPaste).unwrap_or(());
        crossterm::execute!(io::stderr(), cursor::Show).unwrap_or(());
        disable_raw_mode().unwrap_or(());
        original_hook(panic_info);
    }));
}

pub fn setup_terminal() -> eyre::Result<CTTerminal> {
    initialize_panic_handler();
    enable_raw_mode()?;
    let mut stderr: Stderr = io::stderr();
    crossterm::execute!(
        stderr,
        EnterAlternateScreen,
        // EnableMouseCapture,
        cursor::Hide,
        EnableBracketedPaste
    )?;

    let backend: CrosstermBackend<Stderr> = CrosstermBackend::new(stderr);
    let terminal: CTTerminal = Terminal::new(backend)?;

    Ok(terminal)
}

pub fn teardown_terminal(mut terminal: CTTerminal) -> eyre::Result<()> {
    disable_raw_mode()?;
    crossterm::execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        // DisableMouseCapture,
        cursor::Show,
        DisableBracketedPaste
    )?;

    Ok(())
}

fn centered_rect<const PERCENT_X: u16, const PERCENT_Y: u16>(rect: Rect) -> Rect {
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Percentage((100 - PERCENT_Y).div_euclid(2)),
            Constraint::Min(0),
            Constraint::Percentage((100 - PERCENT_Y).div_euclid(2)),
        ])
        .split(rect);

    Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Percentage((100 - PERCENT_X).div_euclid(2)),
            Constraint::Min(0),
            Constraint::Percentage((100 - PERCENT_X).div_euclid(2)),
        ])
        .split(popup_layout[1])[1]
}

fn messages_to_list(messages: &[DisplayMessage]) -> List {
    List::new(messages.iter().map(
        |DisplayMessage {
             ref message_text,
             ref text_colour,
             ref background_colour,
         }|
         -> ListItem {
            ListItem::new(Line::from(Span::styled(
                message_text,
                Style::default().bg(*background_colour).fg(*text_colour),
            )))
        },
    ))
}

fn render_help_popup(frame: &mut Frame<'_>, total_area: Rect) {
    let popup_block: Block = Block::default()
        .style(Style::new().bg(Color::DarkGray))
        .borders(Borders::ALL);

    let help_paragraph: Paragraph =
        Paragraph::new(Text::styled(POPUP_CONTENT, Style::new().fg(Color::Red)))
            .block(popup_block)
            .wrap(Wrap { trim: false });
    let popup_area = centered_rect::<50, 50>(total_area);
    frame.render_widget(Clear, popup_area);
    frame.render_widget(help_paragraph, popup_area);
}

fn block_list_selected<'inner_cow>(
    (current, ((channel, channel_state), rect)): (
        usize,
        ((&String, &'inner_cow ChannelState), &'inner_cow Rect),
    ),
    selected: usize,
) -> (List<'inner_cow>, &'inner_cow Rect) {
    let chat_block: Block = Block::default()
        .title(format!("{} {}", channel, channel_state.format_short()))
        .borders(Borders::ALL);

    if selected == current {
        (
            messages_to_list(&channel_state.messages)
                .block(chat_block.border_style(SELECTED_BORDER)),
            rect,
        )
    } else {
        (
            messages_to_list(&channel_state.messages).block(chat_block),
            rect,
        )
    }
}

fn block_list<'inner_cow>(
    ((channel, channel_state), rect): ((&String, &'inner_cow ChannelState), &'inner_cow Rect),
) -> (List<'inner_cow>, &'inner_cow Rect) {
    let chat_block: Block = Block::default()
        .title(format!("{} {}", channel, channel_state.format_short()))
        .borders(Borders::ALL);
    (
        messages_to_list(&channel_state.messages).block(chat_block),
        rect,
    )
}

pub fn ui_main(frame: &mut Frame<'_>, model: &Model) {
    let total_area = frame.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Min(3), Constraint::Length(1)])
        .split(total_area);
    assert!(chunks.len() > 1, "Constrants specify 2 chunks");

    let channel_count: usize = model.channels.len();
    let percentage: u16 = 100_u16
        .checked_div_euclid(u16::try_from(channel_count).unwrap_or_else(|_| unreachable!()))
        .unwrap_or_default();
    // .unwrap_unchecked();
    // If there are that many channels, there are worse problems to worry about

    let channel_chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints((0..channel_count).map(|_| Constraint::Percentage(percentage)))
        .split(chunks[0]);

    let iterator = model.channels.iter().zip(channel_chunks.iter());
    let CurrentScreen::Main(ref selected_channel) = model.current_screen else {
        unreachable!()
    };
    // Matching to draw channels and pick help note content at the same time
    let help_content: &str = match *selected_channel {
        Some(Chats::SelectedChannel(selected)) => {
            iterator
                .enumerate()
                .map(
                    |tuple: (usize, ((&String, &ChannelState), &Rect))| -> (List, &Rect) {
                        block_list_selected(tuple, usize::from(selected))
                    },
                )
                .for_each(|(message_list, area): (List, &Rect)| {
                    frame.render_widget(message_list, *area);
                });
            HELP_CONTENT
        }
        None => {
            iterator
                .map(block_list)
                .for_each(|(message_list, area): (List, &Rect)| {
                    frame.render_widget(message_list, *area);
                });
            HELP_CONTENT
        }
        Some(Chats::Staging) => {
            iterator
                .map(block_list)
                .for_each(|(message_list, area): (List, &Rect)| {
                    frame.render_widget(message_list, *area);
                });
            HELP_CONTENT_STAGING
        }
    };

    // Line::styled, doesn't actually apply style, isn't it great
    let help_note = Paragraph::new(Line::from(Span::styled(help_content, IMPORTANT)).centered());
    frame.render_widget(help_note, chunks[1]);

    if model.help_popup {
        render_help_popup(frame, total_area);
    }
}

pub fn ui_error(frame: &mut Frame<'_>, model: &Model) {
    let total_area: Rect = frame.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Min(1), Constraint::Length(1)])
        .split(total_area);
    assert!(chunks.len() > 1, "Constraints specify 2 chunks");

    // TODO: get rid of this else or promote to unsafe
    let CurrentScreen::Error(ref error) = model.current_screen else {
        unreachable!()
    };
    let error_paragraph: Paragraph =
        Paragraph::new(Text::styled(error, Style::new().fg(Color::Red)))
            .wrap(Wrap { trim: false })
            .centered();
    let error_area: Rect = centered_rect::<100, 25>(chunks[0]);

    frame.render_widget(Clear, total_area);
    frame.render_widget(error_paragraph, error_area);

    let help_note = Paragraph::new(Line::from(Span::styled(HELP_CONTENT, IMPORTANT)).centered());
    frame.render_widget(help_note, chunks[1]);

    if model.help_popup {
        render_help_popup(frame, total_area);
    }
}

fn button_line<'inner_cow, const IS_ON: bool>() -> Line<'inner_cow> {
    if IS_ON {
        Line::from(vec![
            Span::raw("OFF | "),
            Span::styled("ON", SELECTED_SETTING),
        ])
    } else {
        Line::from(vec![
            Span::styled("OFF", SELECTED_SETTING),
            Span::raw(" | ON"),
        ])
    }
}

fn button_line_timestamps<'inner_cow>(
    current_timestamp_format: TimestampFormat,
) -> Line<'inner_cow> {
    match current_timestamp_format {
        TimestampFormat::DontShow => Line::from(vec![
            Span::styled("Don't show", SELECTED_SETTING),
            Span::raw(" | HH:mm | HH:mm:ss"),
        ]),
        TimestampFormat::HoursMinutes => Line::from(vec![
            Span::raw("Don't show | "),
            Span::styled("HH:mm", SELECTED_SETTING),
            Span::raw("|  HH:mm:ss"),
        ]),
        TimestampFormat::HoursMinutesSeconds => Line::from(vec![
            Span::raw("Don't show | HH:mm | "),
            Span::styled("HH:mm:ss", SELECTED_SETTING),
        ]),
    }
}

fn settings_paragraph<'inner_cow>(
    setting: SelectedSetting,
    current_selection: &SelectedSetting,
    tui_settings: TuiSettings,
    connection_settings: MockConnectionSettings,
) -> Paragraph<'inner_cow> {
    // I could match just `setting` and `current_selection`
    // but surely more match branches is better than inner if branching
    // TODO: split this match into 2 with compile time parameter instead of `setting`
    let setting_text: Vec<Line<'_>> = match (
        setting,
        current_selection,
        tui_settings.get_show_badges(),
        connection_settings.get_log(),
    ) {
        (SelectedSetting::Log, &SelectedSetting::Log, _, true) => vec![
            Line::from(Span::styled(LOG_CONTENT, FOCUSED_SETTING)),
            button_line::<true>(),
        ],
        (SelectedSetting::Log, _, _, true) => vec![Line::raw(LOG_CONTENT), button_line::<true>()],
        (SelectedSetting::Log, &SelectedSetting::Log, _, false) => vec![
            Line::from(Span::styled(LOG_CONTENT, FOCUSED_SETTING)),
            button_line::<false>(),
        ],
        (SelectedSetting::Log, _, _, false) => vec![Line::raw(LOG_CONTENT), button_line::<false>()],
        (SelectedSetting::ShowBadges, &SelectedSetting::ShowBadges, true, _) => vec![
            Line::from(Span::styled(BADGE_CONTENT, FOCUSED_SETTING)),
            button_line::<true>(),
        ],
        (SelectedSetting::ShowBadges, _, true, _) => {
            vec![Line::raw(BADGE_CONTENT), button_line::<true>()]
        }
        (SelectedSetting::ShowBadges, &SelectedSetting::ShowBadges, false, _) => vec![
            Line::from(Span::styled(BADGE_CONTENT, FOCUSED_SETTING)),
            button_line::<false>(),
        ],
        (SelectedSetting::ShowBadges, _, false, _) => {
            vec![Line::raw(BADGE_CONTENT), button_line::<false>()]
        }
        (SelectedSetting::Timestamps(timestamps), &SelectedSetting::Timestamps(_), _, _) => vec![
            Line::from(Span::styled(BADGE_CONTENT, FOCUSED_SETTING)),
            button_line_timestamps(timestamps),
        ],
        (SelectedSetting::Timestamps(timestamps), _, _, _) => {
            vec![Line::raw(BADGE_CONTENT), button_line_timestamps(timestamps)]
        }
    };
    Paragraph::new(setting_text)
        .block(SETTINGS_BLOCK)
        .centered()
}

pub fn ui_settings(frame: &mut Frame<'_>, model: &Model) {
    let total_area: Rect = frame.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Min(3),
            Constraint::Min(3),
            Constraint::Min(3),
            Constraint::Length(1),
        ])
        .split(total_area);
    assert!(chunks.len() > 3, "Contraints specify 4 UI blocks");

    // TODO: get rid of this else or promote to unsafe
    let CurrentScreen::TuiSettings(ref current_selection) = model.current_screen else {
        unreachable!();
    };
    let badges_paragraph: Paragraph = settings_paragraph(
        SelectedSetting::ShowBadges,
        current_selection,
        model.tui_settings,
        model.connection_settings,
    );
    frame.render_widget(badges_paragraph, chunks[0]);
    let log_paragraph: Paragraph = settings_paragraph(
        SelectedSetting::Log,
        current_selection,
        model.tui_settings,
        model.connection_settings,
    );
    frame.render_widget(log_paragraph, chunks[1]);
    let timestamps_paragraph: Paragraph = settings_paragraph(
        SelectedSetting::Timestamps(model.tui_settings.get_timestamps()),
        current_selection,
        model.tui_settings,
        model.connection_settings,
    );
    frame.render_widget(timestamps_paragraph, chunks[2]);

    let help_note: Paragraph<'_> =
        Paragraph::new(Line::from(Span::styled(HELP_CONTENT, IMPORTANT)).centered());
    frame.render_widget(help_note, chunks[3]);

    if model.help_popup {
        render_help_popup(frame, total_area);
    }
}
