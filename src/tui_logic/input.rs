/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use color_eyre::eyre;
use crossterm::event::{Event, EventStream, KeyCode, KeyEvent, KeyEventKind, KeyModifiers};
use futures::StreamExt;
use tokio::{
    sync::mpsc::{error::SendError, UnboundedSender},
    task::JoinHandle,
};
use tokio_util::sync::CancellationToken;

use crate::tui_logic::input_action::{Direction, InputAction};

fn handle_key(
    key_event: KeyEvent,
    input_send: &UnboundedSender<InputAction>,
    token: &CancellationToken,
) -> eyre::Result<()> {
    if key_event.kind != KeyEventKind::Press {
        return Ok(());
    }
    // TODO: more
    match key_event.code {
        KeyCode::Esc => token.cancel(),
        KeyCode::Tab => input_send.send(InputAction::ChangeScreen)?,
        KeyCode::Char('z' | 'Z') if key_event.modifiers == KeyModifiers::CONTROL => {
            input_send.send(InputAction::Cancel)?;
        }
        KeyCode::Char('h' | 'H') if key_event.modifiers == KeyModifiers::CONTROL => {
            input_send.send(InputAction::ToggleHelp)?;
        }
        KeyCode::Char(character) => input_send.send(InputAction::Char(character))?,
        KeyCode::Up => input_send.send(InputAction::MoveFocus(Direction::Up))?,
        KeyCode::Down => input_send.send(InputAction::MoveFocus(Direction::Down))?,
        KeyCode::Left => input_send.send(InputAction::MoveFocus(Direction::Left))?,
        KeyCode::Right => input_send.send(InputAction::MoveFocus(Direction::Right))?,
        KeyCode::Enter => input_send.send(InputAction::Confirm)?,
        KeyCode::Delete | KeyCode::Backspace => input_send.send(InputAction::Backspace)?,
        _ => {}
    }
    Ok(())
}

fn handle_resize(input_send: &UnboundedSender<InputAction>) -> Result<(), SendError<InputAction>> {
    input_send.send(InputAction::Resize)
}

// tokio::select! doesn't allow to just get a reference
#[allow(clippy::needless_pass_by_value)]
fn handle_input(
    ct_event: Event,
    input_send: &UnboundedSender<InputAction>,
    token: &CancellationToken,
) -> eyre::Result<()> {
    match ct_event {
        Event::Key(key_event) => handle_key(key_event, input_send, token)?,
        Event::Resize(_, _) => handle_resize(input_send)?,
        // Their capture isn't enabled
        Event::Paste(_) | Event::FocusGained | Event::FocusLost | Event::Mouse(_) => {}
    }
    Ok(())
}

async fn input_loop(
    input_send: UnboundedSender<InputAction>,
    token: &CancellationToken,
) -> eyre::Result<()> {
    let mut reader: EventStream = EventStream::new();

    loop {
        tokio::select! {
            biased;
            () = token.cancelled() => break,
            Some(ct_event) = &mut reader.next() => {
                handle_input(ct_event?, &input_send, token)?;
            }
        }
    }
    Ok(())
}

#[must_use]
#[inline]
pub fn spawn_input_thread(
    input_send: UnboundedSender<InputAction>,
    cancellation_token: CancellationToken,
) -> JoinHandle<eyre::Result<()>> {
    tokio::spawn(async move {
        let token = cancellation_token;

        let result: eyre::Result<()> = input_loop(input_send, &token).await;
        if result.is_err() {
            token.cancel();
        }
        result
    })
}
