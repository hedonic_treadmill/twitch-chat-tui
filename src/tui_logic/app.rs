/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use color_eyre::eyre;
use core::default::Default;
use indexmap::IndexMap;
use ratatui::{style::Color, Frame};
use tokio::sync::mpsc::UnboundedSender;
use twitch_irc::validate::validate_login;

use crate::{
    connection_logic::ConnectionAction,
    thread_logic::{
        app_settings::SharedSetting,
        sent_messages::{ChatMessage, MiscMessage, SentRoomstate, SystemMessage},
    },
    tui_logic::{
        input_action::{Direction, InputAction},
        tui::{ui_error, ui_main, ui_settings, CTTerminal},
        tui_settings::{MockConnectionSettings, SelectedSetting, TimestampFormat, TuiSettings},
    },
};

const MSG_COUNT: usize = 25;
const EMPTY_STRING: DisplayMessage = DisplayMessage::empty();
const STAGING_NAME: &str = "NO CHANNEL";

#[derive(Clone)]
pub struct DisplayMessage {
    pub message_text: String,
    pub text_colour: Color,
    pub background_colour: Color,
}

impl DisplayMessage {
    pub const fn new(message_text: String, text_colour: Color, background_colour: Color) -> Self {
        Self {
            message_text,
            text_colour,
            background_colour,
        }
    }
    pub const fn empty() -> Self {
        Self {
            message_text: String::new(),
            text_colour: Color::White,
            background_colour: Color::Black,
        }
    }
    pub const fn new_chat(message_text: String) -> Self {
        Self::new(message_text, Color::White, Color::Black)
    }
    pub const fn new_system(message_text: String) -> Self {
        Self::new(message_text, Color::DarkGray, Color::Black)
    }
    /// Only for staged
    pub fn add_char_staged(&mut self, character: char) {
        self.message_text.push(character);
    }
    /// Only for staged
    pub fn delete_char_staged(&mut self) {
        self.message_text.pop();
    }
}

pub enum Chats {
    SelectedChannel(u8),
    Staging,
}

pub enum CurrentScreen {
    Main(Option<Chats>),
    TuiSettings(SelectedSetting),
    Error(String),
}

#[derive(Default)]
pub struct DisplayRoomstate {
    emote_only: bool,
    followers_only_mins: Option<u64>,
    r9k: bool,
    slow_mode_secs: u64,
    sub_only: bool,
}

impl DisplayRoomstate {
    pub fn update(&self, received_roomstate: SentRoomstate) -> Self {
        let emote_only: bool = received_roomstate.emote_only.unwrap_or(self.emote_only);
        let followers_only_mins: Option<u64> = received_roomstate
            .followers_only_mins
            .unwrap_or(self.followers_only_mins);
        let r9k: bool = received_roomstate.r9k.unwrap_or(self.r9k);
        let slow_mode_secs: u64 = received_roomstate
            .slow_mode_secs
            .unwrap_or(self.slow_mode_secs);
        let sub_only: bool = received_roomstate.sub_only.unwrap_or(self.sub_only);
        Self {
            emote_only,
            followers_only_mins,
            r9k,
            slow_mode_secs,
            sub_only,
        }
    }
    pub fn format_short(&self) -> String {
        // TODO: optimize this
        let emote_only: &str = if self.emote_only { "emote" } else { "" };
        let followers_only: String = match self.followers_only_mins {
            Some(0) => "follow".to_owned(),
            Some(mins) => format!("follow({mins}m)"),
            None => String::new(),
        };
        let r9k: &str = if self.r9k { "r9k" } else { "" };
        let slow_mode: String = if self.slow_mode_secs == 0 {
            String::new()
        } else {
            format!("slow({}s)", self.slow_mode_secs)
        };
        let sub_only: &str = if self.sub_only { "sub" } else { "" };
        // to hell with extra spaces
        format!("{emote_only} {followers_only} {r9k} {slow_mode} {sub_only}")
    }
}

pub struct ChannelState {
    pub messages: Vec<DisplayMessage>,
    pub roomstate: DisplayRoomstate,
}

impl ChannelState {
    const fn new(messages: Vec<DisplayMessage>, roomstate: DisplayRoomstate) -> Self {
        Self {
            messages,
            roomstate,
        }
    }
    pub fn update(&mut self, new_message: DisplayMessage) {
        // Quite unfortunate mutability forced by needing to stay withing `IndexMap`
        self.messages.swap_remove(MSG_COUNT - 1);
        self.messages.insert(0, new_message);
    }
    pub fn format_short(&self) -> String {
        self.roomstate.format_short()
    }
    pub fn update_roomstate(&mut self, received_roomstate: SentRoomstate) {
        // Quite unfortunate mutability forced by needing to stay withing `IndexMap`
        self.roomstate = self.roomstate.update(received_roomstate);
    }
    pub fn new_staged() -> Self {
        Self::new(
            vec![
                DisplayMessage::new("Input channel name".to_owned(), Color::White, Color::Black),
                DisplayMessage::new(String::with_capacity(15), Color::White, Color::Black),
                EMPTY_STRING, // This string for error if there is one
            ],
            DisplayRoomstate::default(),
        )
    }
    /// Only for a staged channel,
    /// adds a character to inputted channel
    pub fn add_char_staged(&mut self, character: char) {
        // Quite unfortunate mutability forced by needing to stay withing `IndexMap`
        self.messages[1].add_char_staged(character);
    }
    /// Only for a staged channel
    /// Shows the error in the 3rd message of the chat view
    pub fn show_staged_error(&mut self, error: String) {
        self.messages[2] = DisplayMessage::new(error, Color::White, Color::LightRed);
    }
    /// Only for a staged channel,
    /// adds a character to inputted channel
    pub fn delete_char_staged(&mut self) {
        // Quite unfortunate mutability forced by needing to stay withing `IndexMap`
        self.messages[1].delete_char_staged();
    }
}

impl Default for ChannelState {
    fn default() -> Self {
        Self::new(vec![EMPTY_STRING; MSG_COUNT], DisplayRoomstate::default())
    }
}

pub struct Model {
    pub current_screen: CurrentScreen,
    pub channels: IndexMap<String, ChannelState>,
    pub help_popup: bool,
    pub tui_settings: TuiSettings,
    pub connection_settings: MockConnectionSettings,
}

impl Model {
    pub const fn new(
        current_screen: CurrentScreen,
        channels: IndexMap<String, ChannelState>,
        help_popup: bool,
        tui_settings: TuiSettings,
        connection_settings: MockConnectionSettings,
    ) -> Self {
        Self {
            current_screen,
            channels,
            help_popup,
            tui_settings,
            connection_settings,
        }
    }
    fn update(self, new_message: DisplayMessage, channel_login: &String) -> Self {
        let mut channels: IndexMap<String, ChannelState> = self.channels;
        // `get_mut` should never return `None` because
        // the channel should already be in the `IndexMap` before update on this channel can be called
        // unwrap_or_else(|| unsafe { unreachable_unchecked() })
        channels
            .get_mut(channel_login)
            .unwrap_or_else(|| unreachable!())
            .update(new_message);

        Self { channels, ..self }
    }
    fn change_screen(self, current_screen: CurrentScreen) -> Self {
        Self {
            current_screen,
            ..self
        }
    }
    fn toggle_help(self) -> Self {
        Self {
            help_popup: !self.help_popup,
            ..self
        }
    }
    fn update_roomstate(self, received_roomstate: SentRoomstate, channel_login: &String) -> Self {
        let mut channels: IndexMap<String, ChannelState> = self.channels;
        channels
            .get_mut(channel_login)
            .unwrap_or_else(|| unreachable!())
            .update_roomstate(received_roomstate);
        Self { channels, ..self }
    }
    fn draw_main(self, terminal: &mut CTTerminal) -> eyre::Result<Self> {
        terminal.draw(|frame: &mut Frame<'_>| ui_main(frame, &self))?;
        Ok(self)
    }
    fn draw_settings(self, terminal: &mut CTTerminal) -> eyre::Result<Self> {
        terminal.draw(|frame: &mut Frame<'_>| ui_settings(frame, &self))?;
        Ok(self)
    }
    fn draw_error(self, terminal: &mut CTTerminal) -> eyre::Result<Self> {
        terminal.draw(|frame: &mut Frame<'_>| ui_error(frame, &self))?;
        Ok(self)
    }
    pub fn receive_action(
        self,
        terminal: &mut CTTerminal,
        tui_action: Option<ConnectionAction>,
    ) -> eyre::Result<Self> {
        match (
            &self.current_screen,
            tui_action,
            self.tui_settings.get_show_badges(),
        ) {
            /* Receiving a regular message */
            (
                CurrentScreen::Main(_),
                Some(ConnectionAction::Chat(ChatMessage {
                    server_timestamp,
                    ref channel_login,
                    sender_login,
                    message_text,
                    ..
                })),
                false,
            ) => {
                // Performance is crying, not matching for timestamps in here
                // On the main screen / Without badges
                let timestamp: String = self
                    .tui_settings
                    .get_timestamps()
                    .format_server_timestamp(server_timestamp);
                let display_message_text: String = timestamp + &sender_login + ": " + &message_text;
                let new_message: DisplayMessage = DisplayMessage::new_chat(display_message_text);
                self.update(new_message, channel_login).draw_main(terminal)
            }
            (
                CurrentScreen::Main(_),
                Some(ConnectionAction::Chat(ChatMessage {
                    server_timestamp,
                    ref channel_login,
                    sender_login,
                    message_text,
                    badges,
                    tags,
                })),
                true,
            ) => {
                // On the main screen / With badges
                let timestamp: String = self
                    .tui_settings
                    .get_timestamps()
                    .format_server_timestamp(server_timestamp);
                let display_message_text: String = timestamp
                    + &badges.format_badges()
                    + &tags.format_tags()
                    + &sender_login
                    + ": "
                    + &message_text;
                let new_message: DisplayMessage = DisplayMessage::new_chat(display_message_text);
                self.update(new_message, channel_login).draw_main(terminal)
            }
            (
                _,
                Some(ConnectionAction::Chat(ChatMessage {
                    server_timestamp,
                    ref channel_login,
                    sender_login,
                    message_text,
                    ..
                })),
                false,
            ) => {
                // Not on the main screen / Without badges
                let timestamp: String = self
                    .tui_settings
                    .get_timestamps()
                    .format_server_timestamp(server_timestamp);
                let display_message_text: String = timestamp + &sender_login + ": " + &message_text;
                let new_message: DisplayMessage = DisplayMessage::new_chat(display_message_text);
                Ok(self.update(new_message, channel_login))
            }
            (
                _,
                Some(ConnectionAction::Chat(ChatMessage {
                    server_timestamp,
                    ref channel_login,
                    sender_login,
                    message_text,
                    badges,
                    tags,
                    ..
                })),
                true,
            ) => {
                // Not on the main screen / With badges
                let timestamp: String = self
                    .tui_settings
                    .get_timestamps()
                    .format_server_timestamp(server_timestamp);
                let display_message_text: String = timestamp
                    + &badges.format_badges()
                    + &tags.format_tags()
                    + &sender_login
                    + ": "
                    + &message_text;
                let new_message: DisplayMessage = DisplayMessage::new_chat(display_message_text);
                Ok(self.update(new_message, channel_login))
            }
            /* *************** */
            /* Receiving a notice (e.g. subscribtion) */
            (
                CurrentScreen::Main(_),
                Some(ConnectionAction::System(SystemMessage {
                    message_text,
                    ref channel_login,
                    misc,
                    ..
                })),
                _,
            ) => {
                // On the main screen with chats
                let new_message: DisplayMessage = DisplayMessage::new_system(message_text);
                match misc {
                    Some(MiscMessage::Roomstate(received_roomstate)) => {
                        self.update_roomstate(received_roomstate, channel_login)
                    }
                    None => self,
                }
                .update(new_message, channel_login)
                .draw_main(terminal)
            }
            (
                _,
                Some(ConnectionAction::System(SystemMessage {
                    message_text,
                    ref channel_login,
                    misc,
                    ..
                })),
                _,
            ) => {
                let new_message: DisplayMessage = DisplayMessage::new_system(message_text);
                let new_model: Self = match misc {
                    Some(MiscMessage::Roomstate(received_roomstate)) => {
                        self.update_roomstate(received_roomstate, channel_login)
                    }
                    None => self,
                }
                .update(new_message, channel_login);
                Ok(new_model)
            }
            /* *************** */
            (_, Some(ConnectionAction::Error(error)), _) => {
                let error_message: String = format!("{error:#}");
                self.change_screen(CurrentScreen::Error(error_message))
                    .draw_error(terminal)
            }
            (_, None, _) => Err(eyre::eyre!("Connection was closed unexpectedly")),
        }
    }
    fn toggle_badges(self) -> Self {
        Self {
            tui_settings: self.tui_settings.toggle_badges(),
            ..self
        }
    }
    fn toggle_logging(self) -> Self {
        Self {
            connection_settings: self.connection_settings.toggle_logging(),
            ..self
        }
    }
    fn set_timestamps(self, timestamps: TimestampFormat) -> Self {
        let new_model: Self = Self {
            tui_settings: self.tui_settings.set_timestamps(timestamps),
            ..self
        };
        new_model.change_screen(CurrentScreen::TuiSettings(SelectedSetting::Timestamps(
            timestamps,
        )))
    }
    pub fn receive_input(
        self,
        terminal: &mut CTTerminal,
        input_action: Option<InputAction>,
        settings_send: &UnboundedSender<SharedSetting>,
    ) -> eyre::Result<Self> {
        match (&self.current_screen, input_action) {
            /* Toggle between main and settings */
            (CurrentScreen::Main(_), Some(InputAction::ChangeScreen))
            | (
                CurrentScreen::TuiSettings(SelectedSetting::Log),
                Some(InputAction::MoveFocus(Direction::Up) | InputAction::Char('w' | 'W')),
                // Changing from main to settings with default being badges
                // or switching from another setting to badges
            ) => self
                .change_screen(CurrentScreen::TuiSettings(SelectedSetting::ShowBadges))
                .draw_settings(terminal),
            (CurrentScreen::TuiSettings(_), Some(InputAction::ChangeScreen)) => {
                // Changing from settings to main view
                self.change_screen(CurrentScreen::Main(None))
                    .draw_main(terminal)
            }
            /* *************** */
            /* Stage and unstage a channel */
            (
                CurrentScreen::Main(None | Some(Chats::SelectedChannel(_))),
                Some(InputAction::Char('e' | 'E')),
                // Being on main screen and receiving a command to start adding a channel
            ) => self.stage_channel().draw_main(terminal),
            (CurrentScreen::Main(Some(Chats::Staging)), Some(InputAction::Cancel)) => {
                self.unstage_channel().draw_main(terminal)
            }
            /* *************** */
            /* Receive chars for staged channel */
            (CurrentScreen::Main(Some(Chats::Staging)), Some(InputAction::Char(character))) => {
                // Add characters to channel string from input
                self.char_to_staged(character).draw_main(terminal)
            }
            (CurrentScreen::Main(Some(Chats::Staging)), Some(InputAction::Backspace)) => {
                // Deleting characters before cursor
                self.delete_char_staged().draw_main(terminal)
            }
            /* *************** */
            /* Joining and leaving channels */
            (CurrentScreen::Main(Some(Chats::Staging)), Some(InputAction::Confirm)) => {
                // Validate and join staged channel
                self.validate_staged(settings_send)?.draw_main(terminal)
            }
            (
                CurrentScreen::Main(Some(Chats::SelectedChannel(_))),
                Some(InputAction::Backspace),
            ) => {
                // Part selected channel
                self.part_channel(settings_send)?.draw_main(terminal)
            }
            /* *************** */
            /* Toggle help */
            (CurrentScreen::Main(_), Some(InputAction::ToggleHelp))
            | (
                CurrentScreen::Main(None | Some(Chats::SelectedChannel(_))),
                Some(InputAction::Char('h' | 'H')),
                // Toggle help popup on main screen
                // Accept either just (h) or (Ctrl + h) when in input mode
            ) => self.toggle_help().draw_main(terminal),
            (
                CurrentScreen::TuiSettings(_),
                Some(InputAction::ToggleHelp | InputAction::Char('h' | 'H')),
            ) => self.toggle_help().draw_settings(terminal),
            (
                CurrentScreen::Error(_),
                Some(InputAction::ToggleHelp | InputAction::Char('h' | 'H')),
            ) => self.toggle_help().draw_error(terminal),
            /* *************** */
            /* Navigate main screen */
            (
                CurrentScreen::Main(None | Some(Chats::SelectedChannel(_))),
                Some(InputAction::Char('a' | 'A') | InputAction::MoveFocus(Direction::Left)),
            ) => self.select_channel::<false>().draw_main(terminal),
            (
                CurrentScreen::Main(None | Some(Chats::SelectedChannel(_))),
                Some(InputAction::Char('d' | 'D') | InputAction::MoveFocus(Direction::Right)),
            ) => self.select_channel::<true>().draw_main(terminal),
            /* *************** */
            /* Navigate settings */
            // The switch from log to badges is above
            (
                CurrentScreen::TuiSettings(SelectedSetting::ShowBadges),
                Some(InputAction::MoveFocus(Direction::Down) | InputAction::Char('s' | 'S')), // From badges to log
            )
            | (
                CurrentScreen::TuiSettings(SelectedSetting::Timestamps(_)),
                Some(InputAction::MoveFocus(Direction::Up) | InputAction::Char('w' | 'W')),
                // From timestamps to log
            ) => self
                .change_screen(CurrentScreen::TuiSettings(SelectedSetting::Log))
                .draw_settings(terminal),
            (
                CurrentScreen::TuiSettings(SelectedSetting::Log),
                Some(InputAction::MoveFocus(Direction::Down) | InputAction::Char('s' | 'S')), // From log to timestamps
            ) => self
                .change_screen(CurrentScreen::TuiSettings(SelectedSetting::Timestamps(
                    TimestampFormat::DontShow,
                )))
                .draw_settings(terminal),
            /* *************** */
            /* Toggle settings */
            (
                CurrentScreen::TuiSettings(SelectedSetting::ShowBadges),
                Some(
                    InputAction::Confirm
                    | InputAction::Char(' ' | 'a' | 'A' | 'd' | 'D')
                    | InputAction::MoveFocus(Direction::Left | Direction::Right),
                ),
            ) => self.toggle_badges().draw_settings(terminal),
            (
                CurrentScreen::TuiSettings(SelectedSetting::Log),
                Some(
                    InputAction::Confirm
                    | InputAction::Char(' ' | 'a' | 'A' | 'd' | 'D')
                    | InputAction::MoveFocus(Direction::Left | Direction::Right),
                ),
            ) => {
                settings_send.send(SharedSetting::Log)?;
                self.toggle_logging().draw_settings(terminal)
            }
            // Timestamp buttons look like this
            // Don't show | H:m | H:m:s
            (
                CurrentScreen::TuiSettings(SelectedSetting::Timestamps(TimestampFormat::DontShow)),
                Some(
                    InputAction::Confirm
                    | InputAction::Char(' ' | 'd' | 'D')
                    | InputAction::MoveFocus(Direction::Right),
                ),
            )
            | (
                CurrentScreen::TuiSettings(SelectedSetting::Timestamps(
                    TimestampFormat::HoursMinutesSeconds,
                )),
                Some(InputAction::Char('a' | 'A') | InputAction::MoveFocus(Direction::Left)),
            ) => self
                .set_timestamps(TimestampFormat::HoursMinutes)
                .draw_settings(terminal), // Hm from DS and Hms
            (
                CurrentScreen::TuiSettings(SelectedSetting::Timestamps(
                    TimestampFormat::HoursMinutes,
                )),
                Some(
                    InputAction::Confirm
                    | InputAction::Char(' ' | 'd' | 'D')
                    | InputAction::MoveFocus(Direction::Right),
                ),
            )
            | (
                CurrentScreen::TuiSettings(SelectedSetting::Timestamps(TimestampFormat::DontShow)),
                Some(InputAction::Char('a' | 'A') | InputAction::MoveFocus(Direction::Left)),
            ) => self
                .set_timestamps(TimestampFormat::HoursMinutesSeconds)
                .draw_settings(terminal), // Hms from Hm and DS
            (
                CurrentScreen::TuiSettings(SelectedSetting::Timestamps(
                    TimestampFormat::HoursMinutesSeconds,
                )),
                Some(
                    InputAction::Confirm
                    | InputAction::Char(' ' | 'd' | 'D')
                    | InputAction::MoveFocus(Direction::Right),
                ),
            )
            | (
                CurrentScreen::TuiSettings(SelectedSetting::Timestamps(
                    TimestampFormat::HoursMinutes,
                )),
                Some(InputAction::Char('a' | 'A') | InputAction::MoveFocus(Direction::Left)),
            ) => self
                .set_timestamps(TimestampFormat::DontShow)
                .draw_settings(terminal),
            /* *************** */
            /* Handle resizes */
            // resize and do nothing
            (CurrentScreen::Main(_), Some(InputAction::Resize)) => self.draw_main(terminal),
            (CurrentScreen::TuiSettings(_), Some(InputAction::Resize)) => {
                self.draw_settings(terminal)
            }
            (CurrentScreen::Error(_), Some(InputAction::Resize)) => self.draw_error(terminal),
            /* *************** */
            (_, None) => Err(eyre::eyre!("Input was stopped unexpectedly")),
            (CurrentScreen::Error(_), Some(InputAction::ChangeScreen)) // do nothing because error on screen
            | (_, _) => Ok(self)
        }
    }
    /// There should be a check before this to see if there is already channel being staged
    fn stage_channel(self) -> Self {
        let mut channels: IndexMap<String, ChannelState> = self.channels;
        channels.insert(STAGING_NAME.to_owned(), ChannelState::new_staged());
        let new_model: Self = Self { channels, ..self };
        new_model.change_screen(CurrentScreen::Main(Some(Chats::Staging)))
    }
    /// On cancellation of staging a new channel
    fn unstage_channel(self) -> Self {
        let mut channels: IndexMap<String, ChannelState> = self.channels;
        channels.swap_remove(STAGING_NAME);
        let new_model: Self = Self { channels, ..self };
        new_model.change_screen(CurrentScreen::Main(None))
    }
    /// Only to be called after entering staging mode
    fn char_to_staged(self, character: char) -> Self {
        let mut channels: IndexMap<String, ChannelState> = self.channels;
        channels
            .get_mut(STAGING_NAME)
            .unwrap_or_else(|| unreachable!())
            .add_char_staged(character);
        Self { channels, ..self }
    }
    /// Only to be called in staged mode.
    fn delete_char_staged(self) -> Self {
        let mut channels: IndexMap<String, ChannelState> = self.channels;
        channels
            .get_mut(STAGING_NAME)
            .unwrap_or_else(|| unreachable!())
            .delete_char_staged();
        Self { channels, ..self }
    }
    /// Only to be called in staged mode.
    /// It's unfortuante that separating the side effects of sending channel to another thread
    /// is not simple
    fn validate_staged(self, settings_send: &UnboundedSender<SharedSetting>) -> eyre::Result<Self> {
        let messages: &Vec<DisplayMessage> = &self
            .channels
            .get(STAGING_NAME)
            .unwrap_or_else(|| unreachable!())
            .messages;
        assert!(
            messages.len() > 2,
            "The number of messages is indicated in ChannelState::new_staged"
        );
        let channel_login: &str = &messages[1].message_text;
        if let Err(name_error) = validate_login(channel_login) {
            let mut channels: IndexMap<String, ChannelState> = self.channels;
            let channel_state: &mut ChannelState = channels
                .get_mut(STAGING_NAME)
                .unwrap_or_else(|| unreachable!());
            channel_state.show_staged_error(format!("{name_error:#}"));
            Ok(Self { channels, ..self })
        } else {
            let mut channels: IndexMap<String, ChannelState> = self.channels;
            let new_channel: String = channels
                .swap_remove(STAGING_NAME)
                .unwrap_or_else(|| unreachable!())
                .messages
                .swap_remove(1)
                .message_text;

            settings_send.send(SharedSetting::ChannelJoined(new_channel.clone()))?;
            channels.insert(new_channel, ChannelState::default());

            let new_model: Self = Self { channels, ..self };
            Ok(new_model.change_screen(CurrentScreen::Main(None)))
        }
    }
    fn part_channel(self, settings_send: &UnboundedSender<SharedSetting>) -> eyre::Result<Self> {
        let CurrentScreen::Main(Some(Chats::SelectedChannel(selected))) = self.current_screen
        else {
            unreachable!()
        };
        let mut channels: IndexMap<String, ChannelState> = self.channels;
        let channel_login: String = channels
            .shift_remove_index(usize::from(selected))
            .unwrap_or_else(|| unreachable!())
            .0;

        settings_send.send(SharedSetting::ChannelParted(channel_login))?;

        let new_model: Self = Self { channels, ..self };
        Ok(new_model.change_screen(CurrentScreen::Main(None)))
    }
    /// false - left
    /// true - right
    fn select_channel<const SELECT_DIRECTION: bool>(self) -> Self {
        let channel_len: u8 =
            (u8::try_from(self.channels.len())).unwrap_or_else(|_| unreachable!());
        if channel_len == 0 {
            return self;
        };
        let current: Option<Chats> = if SELECT_DIRECTION {
            // To the right
            match self.current_screen {
                CurrentScreen::Main(None) => Some(Chats::SelectedChannel(0)),
                CurrentScreen::Main(Some(Chats::SelectedChannel(current)))
                    if current + 1 == channel_len =>
                {
                    None
                }
                CurrentScreen::Main(Some(Chats::SelectedChannel(current))) => {
                    Some(Chats::SelectedChannel(current + 1))
                }
                _ => unreachable!(),
            }
        } else {
            // To the left
            match self.current_screen {
                CurrentScreen::Main(None) => Some(Chats::SelectedChannel(channel_len - 1)),
                CurrentScreen::Main(Some(Chats::SelectedChannel(0))) => None,
                CurrentScreen::Main(Some(Chats::SelectedChannel(current))) => {
                    Some(Chats::SelectedChannel(current - 1))
                }
                _ => unreachable!(),
            }
        };
        let current_screen: CurrentScreen = CurrentScreen::Main(current);
        Self {
            current_screen,
            ..self
        }
    }
}

impl Default for Model {
    fn default() -> Self {
        let current_screen: CurrentScreen = CurrentScreen::Main(None);
        // When changing this channel list, sync changes with src/connection_logic.rs
        let channels: IndexMap<String, ChannelState> = indexmap::indexmap! {
            "jerma985".to_owned() => ChannelState::default(),
        };
        let help_popup: bool = false;
        let tui_settings: TuiSettings = TuiSettings::default();
        let connection_settings: MockConnectionSettings = MockConnectionSettings::default();
        Self::new(
            current_screen,
            channels,
            help_popup,
            tui_settings,
            connection_settings,
        )
    }
}
