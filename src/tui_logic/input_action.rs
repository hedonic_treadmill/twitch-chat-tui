/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

pub enum InputAction {
    ChangeScreen,
    MoveFocus(Direction),
    ToggleHelp,
    Confirm,
    Cancel,
    Resize,
    Char(char),
    Backspace,
}
