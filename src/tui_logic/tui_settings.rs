/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use chrono::{DateTime, Utc};
use core::default::Default;

#[derive(Clone, Copy)]
pub enum TimestampFormat {
    DontShow,
    HoursMinutes,
    HoursMinutesSeconds,
}

impl TimestampFormat {
    pub fn format_server_timestamp(self, server_timestamp: DateTime<Utc>) -> String {
        match self {
            Self::DontShow => String::new(),
            Self::HoursMinutes => server_timestamp.format("%H:%M ").to_string(),
            Self::HoursMinutesSeconds => server_timestamp.format("%H:%M:%S ").to_string(),
        }
    }
}

pub enum SelectedSetting {
    ShowBadges,
    Log,
    Timestamps(TimestampFormat),
}

#[derive(Clone, Copy)]
pub struct TuiSettings {
    show_badges: bool,
    timestamps: TimestampFormat,
}

impl TuiSettings {
    pub const fn new(show_badges: bool, timestamps: TimestampFormat) -> Self {
        Self {
            show_badges,
            timestamps,
        }
    }
    pub const fn toggle_badges(self) -> Self {
        Self {
            show_badges: !self.show_badges,
            ..self
        }
    }
    pub const fn get_show_badges(self) -> bool {
        self.show_badges
    }
    pub const fn set_timestamps(self, timestamps: TimestampFormat) -> Self {
        Self { timestamps, ..self }
    }
    pub const fn get_timestamps(self) -> TimestampFormat {
        self.timestamps
    }
}

impl Default for TuiSettings {
    fn default() -> Self {
        Self::new(true, TimestampFormat::DontShow)
    }
}

/// Mock connection settings to keep track for TUI
/// Isn't actually the settings for connection, needs to be synced with the real ones
#[derive(Clone, Copy)]
pub struct MockConnectionSettings {
    log: bool,
}

impl MockConnectionSettings {
    pub const fn new(log: bool) -> Self {
        Self { log }
    }
    pub const fn toggle_logging(self) -> Self {
        Self {
            log: !self.log,
            // ..self
        }
    }
    pub const fn get_log(self) -> bool {
        self.log
    }
}

impl Default for MockConnectionSettings {
    fn default() -> Self {
        // TODO: sync this somehow with the real settings
        // to not accidentally introduce bugs
        // by changing one but not the other
        Self::new(false)
    }
}
