/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use twitch_irc::message::{
    ClearChatMessage, ClearMsgMessage, IRCMessage, NoticeMessage, PrivmsgMessage, RoomStateMessage,
    ServerMessage, UserNoticeMessage,
};

use crate::{
    connection_logic::{
        connection_settings::ConnectionSettings,
        extra_handling::connection_action::ConnectionAction,
        format_extra::format_list_of_names,
        format_messages::{
            format_clear_chat, format_clear_msg, format_notice, format_privmsg, format_roomstate,
            format_user_notice,
        },
        log_message::{log_privmsg, log_system},
    },
    thread_logic::sent_messages::{ChatMessage, SystemMessage},
};

fn handle_privmsg(privmsg: PrivmsgMessage, settings: &ConnectionSettings) -> ConnectionAction {
    let chat_message: ChatMessage = format_privmsg(privmsg);
    if settings.log {
        if let Err(fs_error) = log_privmsg(&chat_message) {
            return ConnectionAction::Error(fs_error);
        }
    }
    ConnectionAction::Chat(chat_message)
}

fn handle_clear_chat(
    clear_chat: ClearChatMessage,
    settings: &ConnectionSettings,
) -> ConnectionAction {
    let system_message: SystemMessage = format_clear_chat(clear_chat);
    if settings.log {
        if let Err(fs_error) = log_system(&system_message) {
            return ConnectionAction::Error(fs_error);
        }
    }
    ConnectionAction::System(system_message)
}

fn handle_clear_msg(clear_msg: ClearMsgMessage, settings: &ConnectionSettings) -> ConnectionAction {
    let system_message: SystemMessage = format_clear_msg(clear_msg);
    if settings.log {
        if let Err(fs_error) = log_system(&system_message) {
            return ConnectionAction::Error(fs_error);
        }
    }
    ConnectionAction::System(system_message)
}

fn handle_notice(notice: NoticeMessage, settings: &ConnectionSettings) -> ConnectionAction {
    let system_message: SystemMessage = format_notice(notice);
    if settings.log {
        if let Err(fs_error) = log_system(&system_message) {
            return ConnectionAction::Error(fs_error);
        }
    }
    ConnectionAction::System(system_message)
}

fn handle_user_notice(
    user_notice: UserNoticeMessage,
    settings: &ConnectionSettings,
) -> ConnectionAction {
    let system_message: SystemMessage = format_user_notice(user_notice);
    if settings.log {
        if let Err(fs_error) = log_system(&system_message) {
            return ConnectionAction::Error(fs_error);
        }
    }
    ConnectionAction::System(system_message)
}

fn handle_roomstate(
    roomstate: RoomStateMessage,
    settings: &ConnectionSettings,
) -> ConnectionAction {
    let system_message: SystemMessage = format_roomstate(roomstate);
    if settings.log {
        if let Err(fs_error) = log_system(&system_message) {
            return ConnectionAction::Error(fs_error);
        }
    }
    ConnectionAction::System(system_message)
}

fn handle_353(irc_message: IRCMessage, settings: &ConnectionSettings) -> Option<ConnectionAction> {
    let system_message: SystemMessage = format_list_of_names(irc_message)?;
    if settings.log {
        if let Err(fs_error) = log_system(&system_message) {
            return Some(ConnectionAction::Error(fs_error));
        }
    }
    Some(ConnectionAction::System(system_message))
}

fn extra_commands(
    server_message: ServerMessage,
    settings: &ConnectionSettings,
) -> Option<ConnectionAction> {
    let irc_message: IRCMessage = IRCMessage::from(server_message);

    /* match irc_message.command.as_str() {
        "001" | "002" | "003" | "004" | "375" | "372" | "376" => format_irc_simple(irc_message),
        "366" => format_end_of_names(irc_message),
        "353" => format_list_of_names(irc_message.params),
        _ => format_irc_all(&irc_message),
    } */
    if irc_message.command.as_str() != "353" {
        return None;
    }
    handle_353(irc_message, settings)
}

#[must_use]
pub fn handle_message(
    server_message: ServerMessage,
    settings: &ConnectionSettings,
) -> Option<ConnectionAction> {
    match server_message {
        ServerMessage::Privmsg(privmsg) => Some(handle_privmsg(privmsg, settings)),
        ServerMessage::Ping(_) | ServerMessage::Pong(_) | ServerMessage::Reconnect(_) => None,
        ServerMessage::ClearChat(clear_chat) => Some(handle_clear_chat(clear_chat, settings)),
        ServerMessage::ClearMsg(clear_msg) => Some(handle_clear_msg(clear_msg, settings)),
        ServerMessage::Notice(notice) => Some(handle_notice(notice, settings)),
        ServerMessage::UserNotice(user_notice) => Some(handle_user_notice(user_notice, settings)),
        ServerMessage::RoomState(roomstate) => Some(handle_roomstate(roomstate, settings)),
        rest => extra_commands(rest, settings),
    }
}
