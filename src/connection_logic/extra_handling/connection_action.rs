/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use color_eyre::eyre;

use crate::thread_logic::sent_messages::{ChatMessage, SystemMessage};

pub enum ConnectionAction {
    Error(eyre::Report),
    System(SystemMessage),
    Chat(ChatMessage),
}
