/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use chrono::{DateTime, Utc};

/* /// `get_utc_timestamp_now`() but also returns the date separately
pub fn get_utc_timestamp_and_date_now() -> (String, String) {
    let utc: DateTime<Utc> = Utc::now();

    (
        utc.format("%Y-%m-%d %H:%M:%S%.6f").to_string(),
        utc.format("%Y-%m-%d").to_string(),
    )
} */

/// Returns timestamp and data as
/// - %Y-%m-%d %H:%M:%S.%f
/// - %Y-%m-%d
///
/// or
/// - yyyy-MM-dd HH:mm:ss.SSSSSS
/// - yyyy-MM-dd
///
/// See <https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior>
/// and <https://docs.rs/chrono/latest/chrono/format/strftime/index.html>
/// and <https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html>
pub fn format_server_timestamp(server_timestamp: DateTime<Utc>) -> (String, String) {
    (
        server_timestamp.format("%Y-%m-%d %H:%M:%S%.6f").to_string(),
        server_timestamp.format("%Y-%m-%d").to_string(),
    )
}
