/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use std::collections::HashMap;

pub struct MessageTags {
    pub new: bool,
    pub highlighted: bool,
    pub returning: bool,
}

impl MessageTags {
    #[must_use]
    #[inline]
    pub fn from_irc_tags(irc_tags: &HashMap<String, Option<String>>) -> Self {
        let highlighted: bool = irc_tags
            .get("msg-id")
            .and_then(|opt: &Option<String>| -> Option<&String> { opt.as_ref() })
            .is_some_and(|value: &String| -> bool { value == "highlighted-message" });

        let new: bool = irc_tags
            .get("first-msg")
            .and_then(|opt: &Option<String>| -> Option<&String> { opt.as_ref() })
            .is_some_and(|value: &String| -> bool { !value.is_empty() && value != "0" });

        let returning: bool = irc_tags
            .get("returning-chatter")
            .and_then(|opt: &Option<String>| -> Option<&String> { opt.as_ref() })
            .is_some_and(|value: &String| -> bool { !value.is_empty() && value != "0" });
        // Length check because can't trust the library and Twitch

        Self {
            new,
            highlighted,
            returning,
        }
    }
    pub fn format_tags(&self) -> String {
        let highlighted: &str = if self.highlighted { "[HL]" } else { "" };
        let new: &str = if self.new { "[NEW]" } else { "" };
        let returning: &str = if self.returning { "[RET]" } else { "" };
        highlighted.to_owned() + new + returning
    }
}
