/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

//! Making a trait for a wrapper of `twitch_irc` client in order to have a mock client
//! for the purpose of testing and benchmarks

use color_eyre::eyre;
use std::future;
use twitch_irc::{
    login::StaticLoginCredentials,
    message::IRCMessage,
    transport::websocket::{WSTransport, TLS},
    TwitchIRCClient,
};

pub trait SendIrcMessage: Sync {
    // Surely this is only for internal use
    fn send_message(
        &self,
        message: IRCMessage,
    ) -> impl future::Future<Output = eyre::Result<()>> + Send;
}

pub struct ClientWrapper {
    pub inner: TwitchIRCClient<WSTransport<TLS>, StaticLoginCredentials>,
}

impl ClientWrapper {
    pub const fn new(inner: TwitchIRCClient<WSTransport<TLS>, StaticLoginCredentials>) -> Self {
        Self { inner }
    }
}

impl SendIrcMessage for ClientWrapper {
    async fn send_message(&self, message: IRCMessage) -> eyre::Result<()> {
        Ok(self.inner.send_message(message).await?)
    }
}
