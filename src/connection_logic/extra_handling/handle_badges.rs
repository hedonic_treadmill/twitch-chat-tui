/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use core::default::Default;
use twitch_irc::message::Badge;

fn reduce_badge_info(accumulator: Option<String>, item: Badge) -> Option<String> {
    if item.name.as_str() != "subscriber" || accumulator.is_some() {
        accumulator
    } else {
        Some(item.version)
    }
}

#[allow(clippy::struct_excessive_bools)]
#[derive(Default)]
pub struct UserBadges {
    pub is_admin: bool,
    pub is_owner: bool,
    pub is_mod: bool,
    pub is_staff: bool,
    pub is_turbo: bool,
    pub sub_months: Option<String>,
}

impl UserBadges {
    #[inline]
    pub fn new(badges: &[Badge], badge_info: Vec<Badge>) -> Self {
        let user_badges: Self = badges.iter().fold(Self::default(), Self::accumulator_fn);
        let sub_months: Option<String> = badge_info.into_iter().fold(None, reduce_badge_info);
        user_badges.set_sub_months(sub_months)
    }
    fn accumulator_fn(self, badge: &Badge) -> Self {
        match badge.name.as_str() {
            "admin" => self.set_admin(true),
            "broadcaster" => self.set_owner(true),
            "moderator" => self.set_mod(true),
            "staff" => self.set_staff(true),
            "turbo" => self.set_turbo(true),
            // "subscriber" => self.set_sub_months(Some(badge.version)),
            // don't match for subscriber because badge_info is more accurate
            _ => self,
        }
    }
    fn set_admin(self, is_admin: bool) -> Self {
        Self { is_admin, ..self }
    }
    fn set_owner(self, is_owner: bool) -> Self {
        Self { is_owner, ..self }
    }
    fn set_mod(self, is_mod: bool) -> Self {
        Self { is_mod, ..self }
    }
    fn set_staff(self, is_staff: bool) -> Self {
        Self { is_staff, ..self }
    }
    fn set_turbo(self, is_turbo: bool) -> Self {
        Self { is_turbo, ..self }
    }
    fn set_sub_months(self, sub_months: Option<String>) -> Self {
        Self { sub_months, ..self }
    }
    pub fn format_badges(&self) -> String {
        let subscriber: String = self
            .sub_months
            .as_ref()
            .map(|months: &String| -> String { format!("[SUB{months}]") })
            .unwrap_or_default();
        let admin: &str = if self.is_admin { "[ADMIN]" } else { "" };
        let moderator: &str = if self.is_mod { "[MOD]" } else { "" };
        let owner: &str = if self.is_owner { "[BC]" } else { "" };
        let staff: &str = if self.is_staff { "[STAFF]" } else { "" };
        let turbo: &str = if self.is_turbo { "[TURBO]" } else { "" };
        subscriber + admin + moderator + owner + staff + turbo
    }
}
