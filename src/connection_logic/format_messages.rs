/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

extern crate alloc;

use alloc::borrow::Cow;
use chrono::prelude::{DateTime, Utc};
use core::time::Duration;
use twitch_irc::message::{
    ClearChatAction, ClearChatMessage, ClearMsgMessage, FollowersOnlyMode, NoticeMessage,
    PrivmsgMessage, RoomStateMessage, UserNoticeMessage,
};

use crate::{
    connection_logic::extra_handling::{handle_badges::UserBadges, handle_tags::MessageTags},
    thread_logic::sent_messages::{
        ChatMessage, MiscMessage, SentFollowersOnlyMode, SentRoomstate, SystemMessage,
    },
};

fn format_followers_only(
    followers_only: &Option<FollowersOnlyMode>,
    trailing_lf: bool,
) -> (Cow<'static, str>, SentFollowersOnlyMode, bool) {
    match (followers_only, trailing_lf) {
        (Some(FollowersOnlyMode::Disabled), true) => (
            Cow::from("This room is no longer in followers-only mode \n"),
            SentFollowersOnlyMode::TurnedOff,
            true,
        ),
        (Some(FollowersOnlyMode::Disabled), false) => (
            Cow::from("This room is no longer in followers-only mode "),
            SentFollowersOnlyMode::TurnedOff,
            true,
        ),
        (Some(FollowersOnlyMode::Enabled(dur)), true) => {
            let minutes: u64 = dur.as_secs().div_euclid(60);
            (
                if minutes == 0 {
                    Cow::from("This room is now in followers-only mode \n")
                } else {
                    Cow::from(format!(
                        "This room is now in {minutes}m followers-only mode \n"
                    ))
                },
                SentFollowersOnlyMode::DelayMins(minutes),
                true,
            )
        }
        (Some(FollowersOnlyMode::Enabled(dur)), false) => {
            let minutes: u64 = dur.as_secs().div_euclid(60);
            (
                if minutes == 0 {
                    Cow::from("This room is now in followers-only mode ")
                } else {
                    Cow::from(format!(
                        "This room is now in {minutes}m followers-only mode "
                    ))
                },
                SentFollowersOnlyMode::DelayMins(minutes),
                true,
            )
        }
        (None, through) => (Cow::default(), SentFollowersOnlyMode::NoChange, through),
    }
}

fn format_slow_mode_duration(
    duration: Duration,
    trailing_lf: bool,
) -> (Cow<'static, str>, Option<u64>, bool) {
    let seconds: u64 = duration.as_secs();
    match (seconds, trailing_lf) {
        (0, true) => (
            Cow::from("This room no longer in slow mode \n"),
            Some(0),
            true,
        ),
        (0, false) => (
            Cow::from("This room no longer in slow mode "),
            Some(0),
            true,
        ),
        (secs, true) => (
            Cow::from(format!(
                "This room now in slow mode. You may send messages every {secs} seconds \n"
            )),
            Some(secs),
            true,
        ),
        (secs, false) => (
            Cow::from(format!(
                "This room now in slow mode. You may send messages every {secs} seconds "
            )),
            Some(secs),
            true,
        ),
    }
}

fn format_slow_mode(
    slow_mode: Option<Duration>,
    trailing_lf: bool,
) -> (Cow<'static, str>, Option<u64>, bool) {
    slow_mode.map_or_else(
        || (Cow::default(), None, trailing_lf),
        |duration: Duration| format_slow_mode_duration(duration, trailing_lf),
    )
}

pub fn format_roomstate(roomstate: RoomStateMessage) -> SystemMessage {
    let server_timestamp: DateTime<Utc> = Utc::now();
    let channel_login: String = roomstate.channel_login;

    // Micro optimizing stripping the extra `\n` at the end with that `bool`
    // The lines are in the opposite of their display order
    // `true` means "Leave a `\n`",
    // `false` means "This is the last line, don't leave a `\n`"
    let sub_only: (&str, Option<bool>, bool) = match roomstate.subscribers_only {
        Some(true) => (
            "This room is now in subscribers-only mode ",
            Some(true),
            true,
        ),
        Some(false) => (
            "This room is no longer in subscribers-only mode ",
            Some(false),
            true,
        ),
        None => ("", None, false),
    };
    let slow_mode: (Cow<'static, str>, Option<u64>, bool) =
        format_slow_mode(roomstate.slow_mode, sub_only.2);
    let r9k: (&str, Option<bool>, bool) = match (roomstate.r9k, slow_mode.2) {
        (Some(true), true) => ("This room is now in unique-chat mode \n", Some(true), true),
        (Some(true), false) => ("This room is now in unique-chat mode ", Some(true), true),
        (Some(false), true) => (
            "This room is no longer in unique-chat mode \n",
            Some(false),
            true,
        ),
        (Some(false), false) => (
            "This room is no longer in unique-chat mode ",
            Some(false),
            true,
        ),
        (None, through) => ("", None, through),
    };
    let followers_only: (Cow<'static, str>, SentFollowersOnlyMode, bool) =
        format_followers_only(&roomstate.follwers_only, r9k.2);
    let emote_only: (&str, Option<bool>) = match (roomstate.emote_only, followers_only.2) {
        (Some(true), true) => ("This room is now in emote-only mode \n", Some(true)),
        (Some(true), false) => ("This room is now in emote-only mode ", Some(true)),
        (Some(false), true) => ("This room is no longer in emote-only mode \n", Some(false)),
        (Some(false), false) => ("This room is no longer in emote-only mode ", Some(false)),
        (None, _) => ("", None),
    };

    let message_text: String = format!(
        "{}{}{}{}{}",
        emote_only.0, followers_only.0, r9k.0, slow_mode.0, sub_only.0,
    );
    let display_roomstate: SentRoomstate = SentRoomstate::new(
        emote_only.1,
        followers_only.1,
        r9k.1,
        slow_mode.1,
        sub_only.1,
    );

    SystemMessage {
        server_timestamp,
        channel_login,
        message_text,
        misc: Some(MiscMessage::Roomstate(display_roomstate)),
    }
}

pub fn format_notice(notice: NoticeMessage) -> SystemMessage {
    let server_timestamp: DateTime<Utc> = Utc::now();
    let channel_login: String = notice.channel_login.unwrap_or_else(|| "_system".to_owned());
    let message_text: String = notice.message_text;
    SystemMessage {
        server_timestamp,
        channel_login,
        message_text,
        misc: None,
    }
}

fn format_chat_cleared(server_timestamp: DateTime<Utc>, channel_login: String) -> SystemMessage {
    let message_text: String = "Chat has been cleared by a moderator".to_owned();
    SystemMessage {
        server_timestamp,
        channel_login,
        message_text,
        misc: None,
    }
}

fn format_user_banned(
    server_timestamp: DateTime<Utc>,
    channel_login: String,
    user_login: String,
) -> SystemMessage {
    let message_text: String = user_login + " has been permanently banned";
    SystemMessage {
        server_timestamp,
        channel_login,
        message_text,
        misc: None,
    }
}

fn format_user_timed_out(
    server_timestamp: DateTime<Utc>,
    channel_login: String,
    user_login: String,
    timeout_length: Duration,
) -> SystemMessage {
    let secs: u64 = timeout_length.as_secs();
    let message_text: String = user_login + " has been timed out for " + &secs.to_string() + "s";
    SystemMessage {
        server_timestamp,
        channel_login,
        message_text,
        misc: None,
    }
}

pub fn format_clear_chat(clear_chat: ClearChatMessage) -> SystemMessage {
    match clear_chat.action {
        ClearChatAction::ChatCleared => {
            format_chat_cleared(clear_chat.server_timestamp, clear_chat.channel_login)
        }
        ClearChatAction::UserBanned { user_login, .. } => format_user_banned(
            clear_chat.server_timestamp,
            clear_chat.channel_login,
            user_login,
        ),
        ClearChatAction::UserTimedOut {
            user_login,
            timeout_length,
            ..
        } => format_user_timed_out(
            clear_chat.server_timestamp,
            clear_chat.channel_login,
            user_login,
            timeout_length,
        ),
    }
}

pub fn format_user_notice(user_notice: UserNoticeMessage) -> SystemMessage {
    let server_timestamp: DateTime<Utc> = user_notice.server_timestamp;
    let channel_login: String = user_notice.channel_login;

    let username: String = user_notice.sender.login;
    let user_text: String = user_notice
        .message_text
        .map_or_else(String::new, |message| username + ": " + &message);
    let message_text: String = user_notice.system_message + &user_text;

    SystemMessage {
        server_timestamp,
        channel_login,
        message_text,
        misc: None,
    }
}

pub fn format_clear_msg(clear_msg: ClearMsgMessage) -> SystemMessage {
    let server_timestamp: DateTime<Utc> = clear_msg.server_timestamp;

    let channel_login: String = clear_msg.channel_login;
    let message_text: String = format!(
        "Message deleted by a moderator: \"{}: {}\"",
        clear_msg.sender_login, clear_msg.message_text,
    );

    SystemMessage {
        server_timestamp,
        channel_login,
        message_text,
        misc: None,
    }
}

pub fn format_privmsg(privmsg: PrivmsgMessage) -> ChatMessage {
    let message_text: String = privmsg.message_text;
    let channel_login: String = privmsg.channel_login;
    let sender_login: String = privmsg.sender.login;
    let server_timestamp: DateTime<Utc> = privmsg.server_timestamp;

    let tags: MessageTags = MessageTags::from_irc_tags(&privmsg.source.tags.0);
    let badges: UserBadges = UserBadges::new(&privmsg.badges, privmsg.badge_info);

    ChatMessage {
        server_timestamp,
        channel_login,
        sender_login,
        message_text,
        badges,
        tags,
    }
}
