/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use color_eyre::eyre;
use core::default::Default;
use tokio::sync::mpsc::UnboundedSender;
use twitch_irc::{login::StaticLoginCredentials, transport::websocket, TwitchIRCClient};

use crate::{
    connection_logic::extra_handling::connection_action::ConnectionAction,
    thread_logic::app_settings::SharedSetting,
};

pub struct ConnectionSettings {
    pub log: bool,
}

impl ConnectionSettings {
    #[must_use]
    pub const fn new(log: bool) -> Self {
        Self { log }
    }

    /// # Errors
    ///
    /// returns `Err` if mpsc channel to another thread returns an error on send
    pub fn reinitialize(
        self,
        client: &TwitchIRCClient<websocket::WSTransport<websocket::TLS>, StaticLoginCredentials>,
        shared_setting: SharedSetting,
        action_send: &UnboundedSender<ConnectionAction>,
    ) -> eyre::Result<Self> {
        let new_settings: Self = match shared_setting {
            SharedSetting::ChannelJoined(channel_login) => {
                // This crate is annoying to work around, I'd really be better off rewriting it myself
                if let Err(channel_error) = client.join(channel_login) {
                    action_send.send(ConnectionAction::Error(eyre::Report::new(channel_error)))?;
                }
                self
            }
            SharedSetting::ChannelParted(channel_login) => {
                client.part(channel_login);
                self
            }
            SharedSetting::Log => Self { log: !self.log },
        };
        Ok(new_settings)
    }
}

impl Default for ConnectionSettings {
    fn default() -> Self {
        // TODO: sync this somehow with the settings shown in TUI
        // to not accidentally introduce bugs
        // by changing one but not the other
        Self::new(false)
    }
}
