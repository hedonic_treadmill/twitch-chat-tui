/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use chrono::prelude::{DateTime, Utc};
use twitch_irc::message::IRCMessage;

use crate::thread_logic::sent_messages::SystemMessage;

const PLACEHOLDER: &str = "_system";
const END_OF_NAMES: &str = "justinfan12345";
const CHAR_LENGTH: usize = 1;

/// Cutting off the hash `#` character
fn crop_channel(channel_login: Option<String>) -> String {
    channel_login.map_or_else(
        || PLACEHOLDER.to_owned(),
        |mut channel: String| -> String { channel.split_off(CHAR_LENGTH) },
    )
}

pub fn format_list_of_names(irc_message: IRCMessage) -> Option<SystemMessage> {
    let server_timestamp: DateTime<Utc> = Utc::now();
    // First is the name of the user
    // Second is an equal sign `=`
    let mut params = irc_message.params.into_iter().skip(2);
    let channel_login: String = crop_channel(params.next());

    let first_name: String = params.next().unwrap_or_else(|| unreachable!());
    if first_name == END_OF_NAMES {
        return None;
    }
    let message_text: String = params.fold(
        first_name,
        |accumulator: String, new_name: String| -> String { accumulator + " " + &new_name },
    );
    Some(SystemMessage {
        server_timestamp,
        channel_login,
        message_text,
        misc: None,
    })
}
/*
pub fn format_irc_simple(irc_message: IRCMessage) -> Option<ConnectionAction> {
    let command = irc_message.command;
    let mut params = irc_message.params.into_iter();

    let message_text: String = format!(
        "{} {} :{}",
        command,
        params.next().unwrap_or_default(),
        params.next().unwrap_or_default(),
    );
    None
}

pub fn format_irc_all(irc_message: &IRCMessage) -> Option<ConnectionAction> {
    let message_text: String = format!("{} {}", irc_message.command, irc_message.params.join(" | "));
    None
}

pub fn format_end_of_names(irc_message: IRCMessage) -> Option<ConnectionAction> {
    let mut params = irc_message.params.into_iter();

    let message_text: String = format!(
        "366 {} {} :{}",
        params.next().unwrap_or_default(),
        params.next().unwrap_or_default(),
        params.next().unwrap_or_default(),
    );
    None
}
 */
