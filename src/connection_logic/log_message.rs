/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use chrono::{DateTime, Days, NaiveDate, Utc};
use color_eyre::eyre;
use lazy_regex::regex_captures;
use std::{
    fs::{self, OpenOptions},
    io::Write,
    path::{Path, PathBuf},
};

use crate::{
    connection_logic::extra_handling::timestamps::format_server_timestamp,
    thread_logic::sent_messages::{ChatMessage, SystemMessage},
};

const CUT_OFF: Days = Days::new(30);

/// Checks whether the filename is a date before the cut off
fn compare_date(date: &str, cut_off_date: &DateTime<Utc>) -> eyre::Result<bool> {
    let naive_date: DateTime<Utc> = NaiveDate::parse_from_str(date, "%Y-%m-%d")?
        .and_hms_opt(0, 0, 0)
        .expect("Replace this with unwrap_unchecked")
        .and_utc();
    Ok(cut_off_date >= &naive_date)
}

/// Used in lib.rs
pub fn clean_old_logs() -> eyre::Result<()> {
    let now: DateTime<Utc> = Utc::now();
    let cut_off_date: DateTime<Utc> = now
        .checked_sub_days(CUT_OFF)
        .ok_or_else(|| eyre::eyre!("Can't subtract date"))?;

    // Not trying if the location isn't available
    let path: &Path = Path::new("messages/");
    if !path.is_dir() {
        return Ok(());
    }

    for entry in fs::read_dir(path)? {
        let temp_path_buf: PathBuf = entry?.path();
        // Value is dropped so temp is needed
        // Skipping to next file if not valid utf-8
        let Some(file) = temp_path_buf.as_path().to_str() else {
            continue;
        };
        // file is of format `messages/{channel_login}_{yyyy-MM-dd}_messages.txt`
        // Stealing the channel_login assumptions from twitch_irc crate
        // but I know this isn't what Twitch actually uses so it may actually
        // let through invalid logins but this isn't a significant problem
        if let Some((_whole, date)) = regex_captures!(
            r"messages\/[a-z0-9_]+?_(\d{4}-\d\d-\d\d)_messages\.txt",
            file
        ) {
            // capturing the yyyy-MM-dd
            /* let date: &str = &caps[1]; */
            if compare_date(date, &cut_off_date)? {
                fs::remove_file(file)?;
            }
        }
    }

    Ok(())
}

pub fn log_privmsg(chat_message: &ChatMessage) -> eyre::Result<()> {
    let (timestamp, date) = format_server_timestamp(chat_message.server_timestamp);
    let filename: String = format!(
        "messages/{}_{}_messages.txt",
        chat_message.channel_login, date
    );

    // Ensure path exists
    fs::create_dir_all("messages")?;
    // OpenOptions for append functionality
    let mut file = OpenOptions::new()
        .create(true)
        .append(true)
        .open(filename)?;

    // TODO: Add badges there as well
    writeln!(
        file,
        "{} | {}{}{}: {}",
        timestamp,
        chat_message.badges.format_badges(),
        chat_message.tags.format_tags(),
        chat_message.sender_login,
        chat_message.message_text,
    )?;

    Ok(())
}

pub fn log_system(system_message: &SystemMessage) -> eyre::Result<()> {
    let (timestamp, date) = format_server_timestamp(system_message.server_timestamp);
    let filename: String = format!(
        "messages/{}_{}_messages.txt",
        system_message.channel_login, date,
    );
    fs::create_dir_all("messages")?;

    let mut file = OpenOptions::new()
        .create(true)
        .append(true)
        .open(filename)?;

    writeln!(file, "{} | {}", timestamp, system_message.message_text)?;
    Ok(())
}
