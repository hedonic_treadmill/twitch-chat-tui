/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

mod connection_logic;
mod thread_logic;
mod tui_logic;

use color_eyre::eyre;
use tokio::{sync::mpsc, task::JoinHandle};
use tokio_util::sync::CancellationToken;

// For benchmarking
pub use crate::connection_logic::{
    handle_message, ConnectionAction, ConnectionSettings, SendIrcMessage,
};
use crate::{
    connection_logic::{clean_old_logs, spawn_connection},
    thread_logic::app_settings::SharedSetting,
    thread_logic::gather_threads,
    tui_logic::{spawn_input_thread, spawn_tui_thread, InputAction},
};

/// # Errors
///
/// Returns `Err` if anything in the application goes wrong
#[inline]
pub async fn run() -> eyre::Result<()> {
    let cancellation_token = CancellationToken::new();

    // TUI channel
    let (action_send, action_recv) = mpsc::unbounded_channel::<ConnectionAction>();
    // Input channel
    let (input_send, input_recv) = mpsc::unbounded_channel::<InputAction>();
    // Change settings from TUI in connection thread
    let (settings_send, settings_recv) = mpsc::unbounded_channel::<SharedSetting>();

    let connection_join_handle: JoinHandle<eyre::Result<()>> =
        spawn_connection(action_send, settings_recv, cancellation_token.clone());
    let input_join_handle: JoinHandle<eyre::Result<()>> =
        spawn_input_thread(input_send, cancellation_token.clone());
    let tui_join_handle: JoinHandle<eyre::Result<()>> = spawn_tui_thread(
        action_recv,
        input_recv,
        settings_send,
        cancellation_token.clone(),
    );

    let result: eyre::Result<()> =
        gather_threads(connection_join_handle, tui_join_handle, input_join_handle).await;
    // Would rather the user not see this tbh
    if let Err(error) = clean_old_logs() {
        println!("{error:#}");
    }
    result
}
