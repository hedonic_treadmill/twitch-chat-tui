/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

mod app;
mod input;
mod input_action;
mod tui;
mod tui_settings;

use color_eyre::eyre;
use tokio::{
    sync::mpsc::{UnboundedReceiver, UnboundedSender},
    task::JoinHandle,
};
use tokio_util::sync::CancellationToken;

// TODO: refactor this pub use
pub use crate::tui_logic::{input::spawn_input_thread, input_action::InputAction};
use crate::{
    connection_logic::ConnectionAction,
    thread_logic::app_settings::SharedSetting,
    tui_logic::{
        app::Model,
        tui::{setup_terminal, teardown_terminal, CTTerminal},
    },
};

/// `Ok`(()) means that `EventStream` received a shutdown signal
///
/// # Errors
///
/// Returns `Err` if something goes wrong with TUI
async fn tui_loop(
    terminal: &mut CTTerminal,
    mut action_recv: UnboundedReceiver<ConnectionAction>,
    mut input_recv: UnboundedReceiver<InputAction>,
    settings_send: UnboundedSender<SharedSetting>,
    token: &CancellationToken,
) -> eyre::Result<()> {
    let mut model: Model = Model::default();

    loop {
        tokio::select! {
            biased;
            () = token.cancelled() => break,
            maybe_action = action_recv.recv() =>
                model = model.receive_action(terminal, maybe_action)?,
            maybe_input = input_recv.recv() =>
                model = model.receive_input(terminal, maybe_input, &settings_send)?,
        }
    }
    Ok(())
}

async fn tui_wrapper(
    action_recv: UnboundedReceiver<ConnectionAction>,
    input_recv: UnboundedReceiver<InputAction>,
    settings_send: UnboundedSender<SharedSetting>,
    token: CancellationToken,
) -> eyre::Result<()> {
    let mut terminal: CTTerminal = setup_terminal()?;

    let tui_result: eyre::Result<()> = tui_loop(
        &mut terminal,
        action_recv,
        input_recv,
        settings_send,
        &token,
    )
    .await;

    if tui_result.is_err() {
        token.cancel();
    }

    teardown_terminal(terminal)?;
    tui_result
}

#[must_use]
#[inline]
pub fn spawn_tui_thread(
    action_recv: UnboundedReceiver<ConnectionAction>,
    input_recv: UnboundedReceiver<InputAction>,
    settings_send: UnboundedSender<SharedSetting>,
    cancellation_token: CancellationToken,
) -> JoinHandle<eyre::Result<()>> {
    tokio::spawn(tui_wrapper(
        action_recv,
        input_recv,
        settings_send,
        cancellation_token,
    ))
}
