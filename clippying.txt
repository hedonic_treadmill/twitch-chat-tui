# Copyright 2024 hedonic_treadmill
# 
# SPDX-License-Identifier: CC0-1.0

As per blanket_clippy_restriction_lints
https://rust-lang.github.io/rust-clippy/master/index.html#blanket_clippy_restriction_lints
the entire `restriction` category shouldn't be bset to deny

So here are the the individial rules that are not enabled when restriction is enabled

restriction = "deny"
question_mark_used = { level = "allow", priority = 1 }  # Idek
implicit_return = { level = "allow", priority = 1 }  #  Not even idiomatic
missing_docs_in_private_items = { level = "allow", priority = 1 }  # This is an application
indexing_slicing = { level = "allow", priority = 1 }  # There are assert checks (hopefully)
unreachable = { level = "allow", priority = 1 }  # unreachable
ref_patterns = { level = "allow", priority = 1 }  # `ref` and `&` don't actually behave the same way
exhaustive_enums = { level = "allow", priority = 1 }  # This is an application
exhaustive_structs = { level = "allow", priority = 1 }  # This is an application
redundant_type_annotations = { level = "allow", priority = 1 }  # rust-analyzer takes up the space of shorter code anyway
arithmetic_side_effects = { level = "allow", priority = 1 }  # It's fine, the operations are not that large
wildcard_enum_match_arm = { level = "allow", priority = 1 }  # In the places it's used, there are too many arms to list them all
pattern_type_mismatch = { level = "allow", priority = 1 }  # I don't understand what it wants from me
self_named_module_files = { level = "allow", priority = 1 }  # Old standard
panic_in_result_fn = { level = "allow", priority = 1 }  # I know what I'm doing
expect_used = { level = "allow", priority = 1 }  # Would rather use unwrap_unchecked
separated_literal_suffix = { level = "allow", priority = 1 }  # Choosing to have underscores
