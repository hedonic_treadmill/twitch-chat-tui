/*
Copyright 2024 hedonic_treadmill

SPDX-License-Identifier: AGPL-3.0-or-later
*/

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use twitch_irc::message::{IRCMessage, ServerMessage};

use twitch_chat_tui::{handle_message, ConnectionAction, ConnectionSettings};

const MSG_STREAM: &str = include_str!("../messages/raw.txt");

fn simulate_line(line: &str, settings: &ConnectionSettings) {
    let Ok(irc_message) = IRCMessage::parse(line) else {
        return;
    };
    let server_message = ServerMessage::try_from(irc_message).unwrap();
    // Do nothing with the value because TUI is not a part of the benchmark
    let _: Option<ConnectionAction> = handle_message(server_message, settings);
}

fn run_lines(stream: &str, settings: &ConnectionSettings) {
    for line in stream.split('\n') {
        simulate_line(line, &settings)
    }
}

pub fn throughput_benchmark(c: &mut Criterion) {
    let settings = ConnectionSettings::new(false);

    c.bench_function("throughput", |b| {
        b.iter(|| run_lines(black_box(MSG_STREAM), black_box(&settings)))
    });
}

criterion_group!(benches, throughput_benchmark);
criterion_main!(benches);
